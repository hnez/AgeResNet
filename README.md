AgeResNet
=========

Using a ResNet CNN for image based age & gender
estimation.

This project was written as part of the
Deep Learning Challenge 2018 at the TU Braunschweig
by Çağrı Çatık, Jonas Mikolajczyk and Leonard Göhrs.

Dependencies
============

The following Arch linux packages and versions
were used during development:

```
python            - 3.6.5-3
python-numpy      - 1.14.5-1
python-pytorch    - 0.4.0-3
python-pillow     - 5.1.0-1
python-matplotlib - 2.2.2-2
python-jinja      - 2.10-1
python-scipy      - 1.1.0-1
opencv            - 3.4.1-3
python-bottle     - 0.12.13-1
```

Reproducibility
===============

While we think that it should be possible to run the
training process from start to completion in one go
and get an usable result we have to admit that
we did not perform our training that way.

We sometimes changed parameters in our code
while training by starting a new training
based on a pretrained network.

Usage
=====

Training and testing of the network is split
into multiple scripts which are documented below


Batch test `batch.py`
---------------------

Takes a list of images of cropped faces from `stdin` and
outputs expected age & gender on `stdout`.

Pass a pre-trained network file using the `-a` switch.

### Example

```
$ find ~/wiki_crop/ -type f | ./batch.py -a contrib/ageresnet1_best_val_loss.bin
~/wiki_crop/00/23804200_1950-03-31_2013.jpg 51 M
~/wiki_crop/00/23836900_1988-10-31_2012.jpg 26 F
~/wiki_crop/00/23882000_1940-06-28_2009.jpg 30 M
~/wiki_crop/00/3386200_1955-02-21_1994.jpg 26 F
```

Metadata Converter `ageresnet/prepare_meta.py`
----------------------------------------------

Takes a IMDB-WIKI `.mat` file and duplicates the
contained metadata into a sqlite database file.
This saves a lot of time spent loading the
`.mat` file on trainer startup.

This takes the destination database as first
parameter and and pairs of `.mat` file and image
root as additional parameters.

### Example

```
$ ./prepare_meta.py meta.db ~/imdb_crop/imdb.mat ~/imdb_crop/ ~/wiki_crop/wiki.mat ~/wiki_crop/
```

Trainer `train.py`
------------------

Trains the network, usually based on a model
pretrained on Imagenet.

### Example

```
$ ./train.py --help
usage: train.py [-h] [-b MINIBATCH] [-l LIMIT] [-i IMAGENET] [-a AGERESNET]
                [-d DATASET] [-u URL] [-o OUTDIR] [-p PREHEAT] [-e EPOCHS]
                [-g GIT]

Group 6 AgeResNet trainer

optional arguments:
  -h, --help            show this help message and exit
  -b MINIBATCH, --minibatch MINIBATCH
                        Minibatch size
  -l LIMIT, --limit LIMIT
                        Limit Dataset to n images
  -i IMAGENET, --imagenet IMAGENET
                        Imagenet base network
  -a AGERESNET, --ageresnet AGERESNET
                        AgeResNet base network
  -d DATASET, --dataset DATASET
                        Dataset Database
  -u URL, --url URL     URL to PUT reports to
  -o OUTDIR, --outdir OUTDIR
                        Output directory
  -p PREHEAT, --preheat PREHEAT
                        Preheating Epoch limit
  -e EPOCHS, --epochs EPOCHS
                        Epoch limit
  -g GIT, --git GIT     Git Commit hash
```

```
$ ./train -b 2 -l 16 -i resnet50-19c8e357.pth -d meta.db
```

Report server `tiny_put.py`
---------------------------

This optional script is used to overcome restrictions
imposed by the lab setup, it provides a http server for
the trainer to `PUT` reports to while training
and for users to `GET` these reports to supervise
the training without having to access the lab.

For this to work the server has to be accessible by
both the training script and the user.

### Example

```
$ pwgen 16 4
Phi4wei1oLeephiW DohcoobohLi0Poop oomoo5cha6ooNudu Ma6aes2eesheghoh

$ ./tiny_put.py Phi4wei1oLeephiW DohcoobohLi0Poop oomoo5cha6ooNudu Ma6aes2eesheghoh
Bottle v0.12.13 server starting up (using WSGIRefServer())...
Listening on http://localhost:18080/
Hit Ctrl-C to quit.
```
