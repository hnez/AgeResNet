"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import os
import sqlite3

import numpy as np
import torch
from PIL import Image
from torchvision import transforms
from torch.utils.data import Dataset


class ImdbWikiDataset(Dataset):
    def __init__(self, database):
        self.conn = sqlite3.connect(database)

        # src: http://blog.outcome.io/pytorch-quick-start-classifying-an-image/
        self.preprocess = transforms.Compose([
            transforms.RandomRotation(15),
            transforms.Resize(190),
            transforms.RandomCrop(160),
            transforms.RandomHorizontalFlip(),
            transforms.ColorJitter(
                brightness=0.1,
                contrast=0.1,
                saturation=0.1,
                hue=0.05,
            ),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            ),
        ])

    def __len__(self):
        c = self.conn.cursor()

        c.execute('SELECT COUNT(*) FROM photos')

        return c.fetchone()[0]

    def __getitem__(self, idx):
        c = self.conn.cursor()

        c.execute('SELECT dob, taken, gender, path FROM photos WHERE id=?', (idx + 1 , ))

        (dob, taken, gender, path) = c.fetchone()

        age = int((taken - dob) / (365 * 24 * 60 * 60) + 0.5)
        age = min(max(age, 0), 99)

        img_pil = Image.open(path).convert('RGB')
        img_tensor = self.preprocess(img_pil)

        label = age + {'M' : 0, 'F': 100}[gender]

        return (img_tensor, label)

    def get_distribution(self, start=0, end=None):
        c = self.conn.cursor()

        if end is None:
            end = len(self)

        c.execute('SELECT dob, taken, gender FROM photos WHERE id>=? AND id <?', (start, end))

        hist = torch.zeros(200)

        for dob, taken, gender in c:
            age = int((taken - dob) / (365 * 24 * 60 * 60) + 0.5)
            age = min(max(age, 0), 99)
            label = age + {'M' : 0, 'F': 100}[gender]

            hist[label] += 1

        hist = hist / hist.sum()

        return hist

class SplitDataset(Dataset):
    def __init__(self, base, start, end):
        self.base = base
        self.start = start
        self.end = end

    def __len__(self):
        return (self.end - self.start)

    def __getitem__(self, idx):
        if idx >= len(self):
            raise IndexError('dataset index out of range')

        return self.base[self.start + idx]

    def get_distribution(self, start=None, length=None):
        if start is None:
            start = self.start

        if length is None:
            length = len(self)

        end = start + length

        return self.base.get_distribution(start, end)

    @classmethod
    def split(cls, dataset, ratio, minimum=0):
        full_len = len(dataset)
        first_len = int(full_len * ratio)

        if first_len < minimum:
            first_len = minimum

        if (full_len - first_len) < minimum:
            first_len = full_len - minimum

        if first_len < minimum:
            raise Exception('SplitDataset could not fulfill minimum of {}'.format(minimum))

        first = cls(dataset, 0, first_len)
        second = cls(dataset, first_len, full_len)

        return (first, second)
