#!/usr/bin/env python3

"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import os
import sys
import random
import math

import datetime as dt
import sqlite3
from scipy.io import loadmat


def matlab_to_datetime(matlab_datenum):
    day = dt.datetime.fromordinal(int(matlab_datenum))
    ts = int(day.timestamp())

    return ts


def year_to_datetime(year):
    ts = int(dt.datetime(year, 6, 1).timestamp())

    return ts


def year_to_seconds(year):
    return 365 * 24 * 60 * 60 * year


def load_mat(conn, path, root_dir, age_blocks=()):
    mat_path = os.path.join(path)
    mat = loadmat(mat_path, squeeze_me=True)
    colname = list(k for k in mat.keys() if not k.startswith('__'))[0]
    data = mat[colname][None][0] # WTF?!

    def prepare(data):
        (dob, taken, path, gender, name, face_score, face2_score) = data

        if math.isnan(gender):
            return None

        ts_dob = matlab_to_datetime(dob)
        ts_taken = year_to_datetime(taken)

        if ts_dob >= ts_taken:
            return None

        if face_score < 0:
            return None

        if face2_score > 0.8:
            # This actually hurts a lot
            return None

        for (start, end) in age_blocks:
            start = year_to_seconds(start)
            end = year_to_seconds(end)
            age = ts_taken - ts_dob

            if (age > start) and (age < end):
                return None

        return (
            ts_dob, ts_taken,
            os.path.join(root_dir, path),
            'F' if float(gender) < 0.5 else 'M',
            str(name), float(face_score), float(face2_score),
        )

    iters = list(zip(data[0], data[1], data[2], data[3], data[4], data[6], data[7]))
    random.shuffle(iters)

    c = conn.cursor()
    c.executemany('''INSERT INTO photos
                     (dob, taken, path, gender, name, face_score, face2_score)
                     VALUES
                     (?,?,?,?,?,?,?)''', (e for e in map(prepare, iters) if e is not None))

    conn.commit()

def create_table(conn):
    c = conn.cursor()
    c.execute('CREATE TABLE photos (id INTEGER PRIMARY KEY, dob INTEGER, taken INTEGER, path TEXT, gender TEXT, name TEXT, face_score REAL, face2_score REAL)')

    conn.commit()


def main():
    if len(sys.argv) < 4:
        print('Usage: {} <Database path> <Matlab file path> <Image Root> [<Matlab file path> <Image Root>] ...'.format(sys.argv[0]))

        return

    db_path = sys.argv[1]
    mat_paths = sys.argv[2::2]
    root_paths = sys.argv[3::2]

    #age_blocks = ((16, 65),)
    age_blocks = tuple()

    conn = sqlite3.connect(db_path)
    create_table(conn)

    for (mat, root) in zip(mat_paths, root_paths):
        print('Loading {} with root {}'.format(mat, root))

        load_mat(conn, mat, root, age_blocks)

    conn.close()


if __name__ == '__main__':
    main()
