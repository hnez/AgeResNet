"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import io
import base64
import time

import torch

import numpy as np

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from jinja2 import Template

from torchvision import transforms

html_template = Template('''<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <title>AgeResNet Report</title>
    <style type="text/css">
      body {
        background-color: #d4d4d4;
      }

      article {
        background-color: #2f2f2f;
        color: white;
        max-width: 50em;
        margin: 2em auto;
        padding: 1em;
        border-radius: 1em;
        box-shadow: 0 0 1em #0006;
      }

      h1 {
        font-size: 1.8em;
        margin-bottom: 0;
        text-decoration: underline;
        font-weight: 500;
      }

      h2 {
        font-size: 1.2em;
        margin: 0.1em 0.5em;
        font-weight: 500;
        color: rgba(255, 255, 255, 0.81);
      }

      h3 {
        margin: 0.1em 1em;
        font-weight: 500;
        color: rgba(255, 255, 255, 0.71);
      }

      p {
        margin-left: 1.5em;
        margin-right: 1em;
      }

      td {
        color: white;
      }
    </style>
  </head>
  <body>
    <article>
      <h1>Meta</h1>

      <h2>Arguments</h2>
      <p>{{ args }}</p>

      <h2>Time</h2>
      <p>
        <table style="width:100%">
          <tr>
            <td>Updated</td>
            <td>{{ ts_now }}</td>
          </tr>
          <tr>
            <td>Expected next</td>
            <td>{{ ts_next }}</td>
          </tr>
        </table>
      </p>
    </article>

    <article>
      <h1>Diagrams</h1>

      <h2>Loss</h2>
      <p><img style="width: 100%" src="data:image/svg+xml;base64,{{ b64svg_losses }}"/></p>

      <h2>AVG Score</h2>
      <p><img style="width: 100%" src="data:image/svg+xml;base64,{{ b64svg_scores }}"/></p>

      <h2>GPU RAM usage</h2>
      <p><img style="width: 100%" src="data:image/svg+xml;base64,{{ b64svg_gpuram }}"/></p>

      <h2>Learning rate</h2>
      <p><img style="width: 100%" src="data:image/svg+xml;base64,{{ b64svg_lr }}"/></p>

      <h2>Time</h2>
      <p><img style="width: 100%" src="data:image/svg+xml;base64,{{ b64svg_time }}"/></p>
    </article>

    <article>
      <h1>Testset results</h1>

      {% for img, label in testset_images %}
      <div style="display: flex; background-color: white; padding: 1em; margin: 1em">
        <img style="flex: 30%; object-fit: contain;" src="data:image/jpg;base64,{{ img }}"/>
        <img style="flex: 70%; object-fit: contain;" src="data:image/svg+xml;base64,{{ label }}"/>
      </div>
      {% endfor %}
    </article>
  </body>
</html>
''')


img_process = transforms.Compose([
    transforms.Normalize(
        mean=[-0.485/0.229, -0.456/0.224, -0.406/0.225],
        std=[1/0.229, 1/0.224, 1/0.225]
    ),
    transforms.ToPILImage(),
])

def plot_vs_epochs(label, *values):
    x = np.arange(len(values[0])) + 1

    y_s = list(np.array(value) for value in values)

    fig = Figure()
    FigureCanvas(fig)

    dims = [x, None] * len(y_s)
    dims[1::2] = y_s

    ax = fig.add_subplot(1, 1, 1)
    ax.plot(*dims)
    ax.set_xlabel('epochs')
    ax.set_ylabel(label)

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64

def plot_ages(male, female):
    x = np.arange(len(male))

    fig = Figure()
    FigureCanvas(fig)

    ax = fig.add_subplot(1, 1, 1)
    ax.plot(x, male, x, female)
    ax.set_xlabel('age')
    ax.set_ylabel('probability')

    buff = io.BytesIO()
    fig.savefig(buff, format='svg')
    b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

    return b64

def convert_images(test_images, test_guess):
    ret = list()

    for i in range(test_images.shape[0]):
        img = img_process(test_images[i,:,:])

        buff = io.BytesIO()
        img.save(buff, 'jpeg')
        b64 = base64.standard_b64encode(buff.getvalue()).decode('utf-8')

        male = test_guess[i,:100].detach().numpy()
        female = test_guess[i,100:].detach().numpy()
        guess = plot_ages(male, female)

        ret.append((b64, guess))

    return ret

def format_ts(ts):
    fmt = time.strftime('%d.%m.%Y - %H:%M', time.localtime(ts))

    return fmt

def gen_report(fname, train_losses, val_losses, train_scores, val_scores,
               gpu_rams, gpu_max_rams, learn_rates,
               test_images, test_guess, args, epoch_times):

    b64svg_losses = plot_vs_epochs('loss', train_losses, val_losses)
    b64svg_scores = plot_vs_epochs('score', train_scores, val_scores)
    b64svg_gpuram = plot_vs_epochs('gpu ram', gpu_rams, gpu_max_rams)
    b64svg_lr = plot_vs_epochs('learn rate', learn_rates)
    b64svg_time = plot_vs_epochs('time', epoch_times)

    testset_images = convert_images(test_images, test_guess)

    ts_now = time.time()
    ts_next = ts_now + epoch_times[-1]

    html = html_template.render(
        b64svg_losses=b64svg_losses,
        b64svg_scores=b64svg_scores,
        b64svg_gpuram=b64svg_gpuram,
        b64svg_lr=b64svg_lr,
        b64svg_time=b64svg_time,
        testset_images=testset_images,
        args=args,
        ts_now=format_ts(ts_now),
        ts_next=format_ts(ts_next),
    )

    with open(fname, 'w') as fd:
        fd.write(html)


def put_file(fname, content_type, url):
    try:
        import requests

        with open(fname, 'rb') as fd:
            headers = {'Content-Type' : content_type}
            requests.put(url, data=fd, headers=headers)

    except Exception as e:
        print('e PUT failed:', e)


def find_accelerator():
    use_cuda = torch.cuda.is_available()
    device = torch.device('cuda' if use_cuda else 'cpu')

    return device


class RelTime(object):
    def __init__(self):
        self.start = time.time()

    def delta(self):
        return (time.time() - self.start)
