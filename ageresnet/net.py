"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.models as models

class AgeResNet(models.resnet.ResNet):
    def __init__(self, imagenet_base=None, ageresnet_base=None):
        block= models.resnet.Bottleneck

        super(AgeResNet, self).__init__(block, [3, 4, 6, 3])

        if imagenet_base:
             self.load_state_dict(torch.load(imagenet_base, map_location=lambda storage, loc: storage))

        # The size of the provided images is 160x160 pixels
        # The origignal ResNet paper uses an image size of 224x224
        # pixels.
        # The original ResNet performs two resolution halving steps
        # in the first two layers to get to 112x112 pixels and finally
        # 56x56 pixels.
        # These two layers are replaced by one that retains the original
        # size and one that scales down by a factor of three.

        # Take 160x160 images, convolve with 7x7 kernels, pad to 174x174
        # get 168x168 images
        self.conv1.stride = 1
        self.conv1.padding = 7

        # Take 168x168 images, pool with 3x3 kernel, pad to 170x170
        # get 56x56 images
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=3, padding=1)

        # The original ResNet has 1000 output classes,
        # we need only 200 for 100 ages x 2 genders
        self.fc = nn.Linear(512 * block.expansion, 200)

        self.dropout1d = nn.Dropout(0.25)

        if ageresnet_base:
            self.load_state_dict(torch.load(ageresnet_base, map_location=lambda storage, loc: storage))

        self.set_mode('test')

    def out_act_log(self, x):
        return (nn.functional.logsigmoid(x) - x.sigmoid().sum().log())

    def out_act(self, x):
        return self.out_act_log(x).exp()

    def set_mode(self, mode):
        for param in self.parameters():
            param.requires_grad = False

        for param in self.select_parameters(mode):
            param.requires_grad = True

        self.mode = mode

    def select_parameters(self, mode):
        for name, param in self.named_parameters():
            if mode not in ('bake', 'preheat', 'test'):
                raise Exception('Unkown train mode {}'.format(mode))

            if (mode == 'bake'):
                yield param

            elif (mode == 'preheat') and (name in ('fc.weight', 'fc.bias')):
                yield param

    def save(self, path):
        torch.save(self.state_dict(), path)

    def forward(self, x):
        do_dropout = self.mode in ('bake', 'preheat')

        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)

        x = self.dropout1d(x) if do_dropout else x

        x = self.fc(x)

        return x
