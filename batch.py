#!/usr/bin/env python3

"""
This file is part of AgeResNet
Copyright (C) 2018 Çağrı Çatık, Jonas Mikolajczyk, Leonard Göhrs

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


import argparse
import sys

from PIL import Image
from ageresnet.test import Tester

def main():
    parser = argparse.ArgumentParser(description='Group 6 AgeResNet tester')

    parser.add_argument('-a', '--ageresnet', help='AgeResNet base network', required=True, type=str)

    args = parser.parse_args()

    tester = Tester(args.ageresnet)

    for path in sys.stdin:
        path = path.strip()

        img_pil = Image.open(path)

        age, gender = tester.test_pil(img_pil)

        print('{} {} {}'.format(path, age, gender))

if __name__ == '__main__':
    main()
